// The questions are as follows:

    
// What directive is used by Node.js in loading the modules it needs?
    // Answer: npm init?

// What Node.js module contains a method for server creation?
    // Answer: http module
// What is the method of the http object responsible for creating a server using Node.js?
    // Answer: createServer(requestListener()) method (function)
// What method of the response object allows us to set status codes and content types?
    // Answer:  response.writeHead
// Where will console.log() output its contents when run in Node.js?
    // Answer: response.write
// What property  of the request object contains the address' endpoint?
    // Answer: url?



const HTTP = require("http");

HTTP.createServer((request, response) => {

    if (request.url === "/login"){
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.write("Welcome to the login page.");
        response.end()
    }else{
        response.writeHead(400, {"Content-Type": "text/plain"});
        response.write("Error");
        response.end();
    }

    
}).listen(4000)